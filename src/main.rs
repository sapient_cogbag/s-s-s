//! Simple static site generator host/theming

pub mod subcommands;
pub mod config;
pub mod mmd;
pub mod page;
mod iterutil;

branch_subcommand!{
    /// Main command
    pub Main,
    "sss",
    "Simple semi-static site generator/manager/hoster program",
    branch {
        subcommands::config::Config;
        subcommands::mmd::MMD;
    }
}

fn main() {
    let cmd = subcommands::make_branch_subcommand::<Main>();
    // take the first (exe) argument away then run commands
    let main_args = std::env::args().skip(1);
    subcommands::run_command(&cmd, Box::new(main_args));
}


// s-s-s
// Copyright (C) 2021  sapient_cogbag <sapient_cogbag at protonmail.com>
//                                                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//                                                                       
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//                                                                       
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
