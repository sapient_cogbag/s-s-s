//! Module for each of the individual pages nya

use std::collections::{HashSet, HashMap};
use crate::mmd;
use std::path::Path;
use std::ffi::{OsStr, OsString};
use crate::iterutil;

pub trait Page {
    /// Get the tags of the page as from the [normalise_tag] function
    fn normalised_tags(&self) -> &HashSet::<String>;

    /// Get the actual title of the page
    fn title(&self) -> &str;
}

/// Holds one site post page and it's title/tags/etc. nya
pub struct HTMLPage {
    /// The html inside the post's <body></body> tags.
    core_html: String,
    /// Holds the title of the page - this is simple plain text nya
    /// (it gets fully delimited)
    title: String,
    /// Tags - these are lowercase and stripped of whitespace
    tags: HashSet::<String>
}


/// Take a tag and normalise into standard form nya
///
/// (strip whitespace then convert to lowercase)
pub fn normalise_tag(raw_tag: &str) -> String {
    raw_tag.trim().to_lowercase()
} 


impl HTMLPage {
    pub fn new(
        document: &mmd::repr::HTMLSanitizedDocument,
        title: String,
        unnormalised_tags: Vec::<String>
    ) -> Self {
        let mut core_html = String::new();
        document.append_html_to_string(&mut core_html);

        Self {
            core_html,
            title,
            tags: unnormalised_tags.into_iter()
                .map(|a| normalise_tag(&a))
                .collect()
        }
    }
}

impl Page for HTMLPage {
    fn normalised_tags(&self) -> &HashSet::<String> { 
        &self.tags
    }

    fn title(&self) -> &str {
        &self.title
    }
}

/// This represents the tree of contents nya
pub enum PageTree <PageType: Page>{
    Page(PageType),
    Directory(HashMap::<OsString, PageTree::<PageType>>)
}

impl <PageType: Page> PageTree::<PageType> {
    /// Make an empty tree which is a root directory
    pub fn root() -> Self {
        Self::Directory(HashMap::new()) 
    }


    /// Make this an empty directory in-place.
    pub fn empty_directory(&mut self) {
        *self = Self::root();
    }

    /// In the case of this being a directory, add or set the given entry
    /// In the case of being a page/file, erase the given file, set to directory,
    ///  then add this entry nya.
    ///
    /// * `entry_name` must not contain any path separators. If it does, then unspecified behaviour
    ///  likely to occur (namely, the entry will become inaccessible nya)
    ///
    /// * `entry` is the entry to put in place.
    ///
    pub fn set_entry(&mut self, entry_name: impl AsRef::<OsStr>, entry: Self) {
        match self {
            PageTree::Page(_) => {
                self.empty_directory();
                // Make this a directory and then run through again nya
                self.set_entry(entry_name, entry);
            }
            PageTree::Directory(mp) => {
                mp.insert(entry_name.as_ref().to_owned(), entry);
            }
        }
    }

    /// This sets an entry at a given path, overwriting any preexisting one nya
    ///
    /// If at any level a page exists where there is a directory in the actual 
    /// path specified here, then the page is deleted and overwritten with a new 
    /// directory.
    /// 
    /// If at the final endpoint anything exists (directory or page) it gets replaced
    /// by the new entry.
    pub fn set_entry_recursive(&mut self, path: impl AsRef::<Path>, entry: Self) {
        let (next_component, components) = iterutil::eat_while(
            path.as_ref().components(),
            |a| match a {
                std::path::Component::Normal(_) => false,
                _ => true
            });
        match next_component {
            // The path points to ourselves - set ourselves nya
            None => {*self = entry;},
            Some(std::path::Component::Normal(next_component)) => {
                match self {
                    // In this case we need to convert to a directory nyaeow,
                    // for adding entries.
                    PageTree::Page(_) => {
                        *self = PageTree::Directory(std::iter::once(
                                // Performance consideration: HashMap::new() does not
                                // allocate nya, so if it gets replaced its fine
                            (next_component.to_owned(), {
                                let mut inner = PageTree::Directory(HashMap::new());
                                // Recurse!
                                inner.set_entry_recursive(components.as_path(), entry);
                                inner
                            }) 
                        ).collect());
                    },
                    // We are a directory
                    PageTree::Directory(entries) => {
                        // Ensrue there's a default value in here nya, then 
                        // recurse!
                        entries.entry(next_component.to_owned()).or_insert(
                            PageTree::Directory(HashMap::new())
                        ).set_entry_recursive(components.as_path(), entry) ;  
                    }
                }
                
            },
            Some(_) => unreachable!("Non-Normal regions got ate >.< nya")
        }
    }

    pub fn is_page(&self) -> bool {
        match self {
            PageTree::Page(_) => true,
            PageTree::Directory(_) => false
        }
    }

    pub fn is_dir(&self) -> bool {
        match self {
            PageTree::Page(_) => false,
            PageTree::Directory(_) => true
        }
    }


    /// Try and locate the page with the given path relative
    /// to our current location nya
    ///
    /// e.g. echo/hello looks for echo in the
    /// current directory and if it is a directory, looks for hello in that.
    /// 
    /// Similar to [PageTree::get_page_or_dir]
    pub fn get_page(&self, path: impl AsRef::<Path>) -> Option::<&PageType> {
        self.get_page_or_dir(path)
            .map(|res| match res {
                PageTree::Page(page) => Some(page),
                PageTree::Directory(_) => None
            })
            .flatten()
    }



    /// Try and locate the page or directory of the given 
    /// path relative to the current location nya.
    pub fn get_page_or_dir(&self, path: impl AsRef::<Path>) -> Option::<&Self> {
        // get the components nya. In particular, this eats all
        // the non-normal components nya
        let (next_component, components) = iterutil::eat_while(
            path.as_ref().components(),
            |a| match a {
                std::path::Component::Normal(_) => false,
                _ => true
            });
        match next_component {
            // No next component! that means we are looking at OURSELVES! nya
            None => Some(self),
            // There is a normal next component. If we are a directory, we 
            // want to look in there nya
            //
            // If we are not a directory then we return None
            Some(std::path::Component::Normal(component)) => match self {
                Self::Page(_) => None,
                // Locate the component in the map nya. If it does not exist 
                // then we cannot find it. Else we pass on the remainder of the 
                // &Path to the next to come before us!! nya
                Self::Directory(contents) => match contents.get(component) {
                    None => None,
                    // The next component does exist so we can dump the rest of 
                    // the path on it ;p
                    Some(tree) => tree.get_page_or_dir(components.as_path())
                } 
            },
            Some(_) => unreachable!("We already ate all the non-Normal components nya")
        }
    }
}

impl <PageType: Page, const LEN: usize> From::<[(&str, PageTree::<PageType>); LEN]> 
for PageTree::<PageType> {
    fn from(input: [(&str, PageTree::<PageType>); LEN]) -> Self {
        let vec = Vec::from(input);
        PageTree::Directory(vec.into_iter().map(|(a, b)| (a.into(), b)).collect())
    }
}

impl <PageType: Page> From::<PageType> for PageTree::<PageType> {
    fn from(v: PageType) -> Self {
        PageTree::Page(v)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_normalise_tag() {
        assert!(normalise_tag("PlAIn") == "plain");
        assert!(normalise_tag(" plENTY") == "plenty");
        assert!(normalise_tag("run for the HILLS! ") == "run for the hills!");
        assert!(normalise_tag(" YaY ") == "yay");
    }

    mod page_tree {
        use super::super::*;

        #[derive(Default)]
        struct DummyPage {
            tags: HashSet::<String>,
            title: &'static str
        }

        impl Page for DummyPage {
            fn normalised_tags(&self) -> &HashSet::<String> {
                &self.tags
            }

            fn title(&self) -> &str {
                self.title
            }
        }

        #[test]
        /// Test for accessing through a page tree by path
        pub fn test_accessing() {
            let tree = PageTree::<DummyPage>::from ([
                ("run", DummyPage::default().into()),
                ("eat", PageTree::from([
                    ("food", PageTree::from(DummyPage::default())),
                    ("metal", PageTree::from(DummyPage::default()))
                ]))
            ]);
            
            let root_page = tree.get_page("/");
            assert!(root_page.is_none());

            let root_page_dir = tree.get_page_or_dir("/");
            assert!(root_page_dir.is_some() && root_page_dir.unwrap().is_dir());
            
            let run_page = tree.get_page("/run");
            assert!(run_page.is_some());
            

            let walk_page_dir = tree.get_page_or_dir("/walk");
            assert!(walk_page_dir.is_none());

            let food_page_dir = tree.get_page_or_dir("/eat/food");
            assert!(food_page_dir.is_some() && food_page_dir.unwrap().is_page());
        }
    }
}
