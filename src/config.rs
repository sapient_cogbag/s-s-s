//! Simple structures for loading/parsing configs

use toml;
use xdg;
use std::iter::Iterator;

pub const XDG_CONFIG_FILE_NAME: &'static str = "sssconfig.toml";
pub const DEFAULT_LOCAL_CONFIG_FPATH: &'static str = "./sssconfig.toml";

macro_rules! glob_from_toml {
    (source $fname:ident matching ($key:expr, $value:ident): $($curr_key:literal => $type:ident $self:ident.$member:ident);*) => {
        match $key { $(
             $curr_key => if let toml::Value::$type(val) = $value {
                $self.$member = val.clone();
            } else {
                eprintln!("WARN [{}]: Expected {} for key {}", $fname, stringify!{$type}, $curr_key);
            },)*
            _ => {}
        }
    }
}


/// Holds configuration for editing site pages
///
/// This can be overwritten by local config but in practise
/// users tend to have the EDITOR environment variable which overwrites even
/// this.
pub struct Edit {
    pub editor_command: String
}

impl Edit {
    /// Overwrite the value from environment variables if it can
    ///
    /// Errors are naturally ignored here since it is just an environment 
    /// override in case you can actually do it
    pub fn override_from_environment(&mut self) {
        if let Ok(editor) = std::env::var("EDITOR") {
            self.editor_command = editor;
        }
    } 

    /// Overwrite from a given toml map.
    ///
    /// Warn on invalid value types
    pub fn override_from_toml(
        &mut self, 
        fname: &str, 
        toml_map: &toml::map::Map::<String, toml::Value>
    ) { 
        for (key, value) in toml_map {
            glob_from_toml!{
                source fname matching (key.as_str(), value):
                    "edit.editor_command" => String self.editor_command
            }
        }
    }

    pub fn as_kv_text(&self) -> String {
        format!{
            r#"edit.editor_command = {}"#,
            self.editor_command
        }
    }

    /// Spawn an editor with the given file to edit nya
    pub fn edit_file(&self, file: &str) -> std::io::Result::<std::process::ExitStatus> {
        std::process::Command::new(self.editor_command.as_str())
            .arg(file)
            .status()
    }
}

impl Default for Edit {
    fn default() -> Self {
        Self {
            editor_command: "vi".to_string()
        }
    }
}

/// Sites do not hold raw HTML, only our mini markup language 
/// for simplicity. HTML and theming/CSS is generated and held purely
/// in memory for access speed (unless it is chosen to be dumped
/// out into a directory).
///
/// Site parameters can be overriden by local config and that then by
/// environment variables.
pub struct Site {
    /// Site directory as a string - note that this is to-be-expanded
    pub site_directory: String,

    /// The name of the site itself
    pub site_name: String,

    /// Site author
    pub site_author: String
}

impl Site { 
    /// Overwrite the values from environment variables if it can
    ///
    /// Errors are naturally ignored here since it is just an environment 
    /// override in case you can actually do it. nya
    ///
    /// Note that the environment variables here are prefixed with SSS_SITE_...
    pub fn override_from_environment(&mut self) {
        if let Ok(site_directory) = std::env::var("SSS_SITE_DIRECTORY") {
            self.site_directory = site_directory;
        }

        if let Ok(site_name) = std::env::var("SSS_SITE_NAME") {
            self.site_name = site_name;
        } 

        if let Ok(site_author) = std::env::var("SSS_SITE_AUTHOR") {
            self.site_author = site_author;
        }
    }
    
    /// Overwrite from a given toml map.
    ///
    /// Warn on invalid value types
    pub fn override_from_toml(
        &mut self, 
        fname: &str, 
        toml_map: &toml::map::Map::<String, toml::Value>
    ) { 
        for (key, value) in toml_map {
            glob_from_toml!{
                source fname matching (key.as_str(), value):
                    "site.site_directory" => String self.site_directory;
                    "site.site_name" => String self.site_name;
                    "site.site_author" => String self.site_author
            }
        }
    }

    pub fn as_kv_text(&self) -> String {
        format!{
            r#"site.site_directory = {}
site.site_author = {}
site.site_name = {}"#,
            self.site_directory,
            self.site_author,
            self.site_name
        }
    }

}

impl Default for Site {
    fn default() -> Self {
        Self {
            site_directory: "~/simple-site".to_string(),
            site_name: "".to_string(),
            site_author: "".to_string()
        }
    }
}

/// Holds the full configuration
#[derive(Default)]
pub struct Config {
   /// Site configuration
   pub site: Site,
   /// Editor configuration (for convenience)
   pub editor: Edit
}

impl Config {
    /// Overwrite appropriate values from appropriate environment variables.
    pub fn override_from_environment(&mut self) {
        self.site.override_from_environment();
        self.editor.override_from_environment();
    }
    
    /// Override from toml config files.
    pub fn override_from_toml(
        &mut self, 
        fname: &str, 
        toml_map: &toml::map::Map::<String, toml::Value>
    ) {
        self.editor.override_from_toml(fname, toml_map);
        self.site.override_from_toml(fname, toml_map);
    }

    pub fn to_string(&self) -> String {
        format!{
            r#"[edit]
{}

[site]
{}"#,
            self.editor.as_kv_text(),
            self.site.as_kv_text()
        }
    }
}


const VALID_KEYS: [&'static str; 4] = [
    "edit.editor_command",

    "site.site_directory",
    "site.site_name",
    "site.site_author"
];


/// Warn on unknown keys in config toml
fn warn_on_unknown_keys(filename: &str, keys_in_toml: toml::map::Keys::<'_>) {
    keys_in_toml.filter(|key| {
        !VALID_KEYS.contains(&key.as_str()) 
    }).for_each(|unknown_key| {
        eprintln!("WARN [{}]: Unknown key '{}' provided.", filename, unknown_key);
    });
} 


/// Attempt to load the configs in priority order
///
/// This should generally not fail at all unless something goes seriously wrong
/// (like environment variables just not existing or something)
///
/// This uses priorities/overrides, and also allows for the 
/// specification of a different "final local config file"
///
/// Note that this logs to stderr for config loading.
pub fn load_config(
    xdg_environ: &Option::<xdg::BaseDirectories>, 
    local_config_file: Option::<&str>
) -> Config {
    let mut cumulative_cfg = Config::default();
    // In order of lowest to highest priority.
    xdg_environ
        .as_ref()
        .map(|a| a.find_config_files(XDG_CONFIG_FILE_NAME))
        .iter_mut().flatten() // Pull out the config files if they exist nyaaa
        .chain(std::iter::once(
            std::path::PathBuf::from(local_config_file.unwrap_or(DEFAULT_LOCAL_CONFIG_FPATH))
        )).for_each(|config_file: std::path::PathBuf| {
            let fname = config_file.to_str().unwrap_or("");
            // Attempt to read file
            match std::fs::read_to_string(&config_file) {
                Ok(config_file_contents) => {
                    let kv = toml::from_str::<
                        '_, 
                         toml::map::Map::<String, toml::Value>
                    >(&config_file_contents);
                    
                    // Handle parsing errors.
                    match kv {
                        Ok(config_kvs) => {
                            // Warn unknown keys
                            warn_on_unknown_keys(fname, config_kvs.keys());

                            // Toml override nyaaaaa
                            cumulative_cfg.override_from_toml(fname, &config_kvs);
                        }
                        Err(parse_err) => {
                            eprintln!(
                                "WARN [{}]: Failed to parse config file as toml: {}", 
                                fname,
                                parse_err.to_string()
                            );
                        }
                    }
                }
                Err(error) => {
                    eprintln!(
                        "WARN [{}]: Failed to open config file: {}", 
                        fname,
                        error.to_string()
                    );
                }
            }
        });
    // Load from environment variables too nya
    cumulative_cfg.override_from_environment();
    cumulative_cfg
}

const DEFAULT_LOCAL_CONFIG: &'static str = include_str!("default_local_config.toml");

/// Create a local config file (default config) in the
/// standard location for current directory (./sss_config.toml)
/// or another local config as specified.
///
/// Requires instruction to overwrite nyaa
pub fn make_local_config(
    local_config_file: Option::<&str>, 
    overwrite_if_exists: bool
) -> Result::<(), String> {
    let path: std::path::PathBuf = local_config_file.unwrap_or(DEFAULT_LOCAL_CONFIG_FPATH).into();
    let does_exist = path.exists();
    if !does_exist || overwrite_if_exists {
        std::fs::write(&path, DEFAULT_LOCAL_CONFIG.as_bytes()).map_err(
            |a| format!("ERR [{}]: {}", path.to_str().unwrap_or(""), a.to_string())
        )?;
        Ok(())
    } else {
        Err(format!("ERR [{}]: File exists.", path.to_str().unwrap_or("")))
    }

}

/// Attempt to get the xdg basedirs nya
pub fn get_xdg() -> Option::<xdg::BaseDirectories> {
    xdg::BaseDirectories::with_prefix("sss").ok()
}


// s-s-s
// Copyright (C) 2021  sapient_cogbag <sapient_cogbag at protonmail.com>
//                                                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//                                                                       
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//                                                                       
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
