///! Utilities for actually running through iterators in-place.
use std::iter;

/// Trait for allowing preview of one ahead into an iterator.
///
/// Analogous to std::iter::Peekable but more generic (`itertools` package for
/// instance nya)
pub trait Previewable : iter::Iterator {
    /// View one ahead in the iterator
    fn preview(&mut self) -> Option::<&<Self as iter::Iterator>::Item>;
}

impl <T: iter::Iterator> Previewable for iter::Peekable::<T> {
    #[inline]
    fn preview(&mut self) -> Option::<&<Self as iter::Iterator>::Item> {
        self.peek()
    }
}

/// Consume elements from a [previewable iterator](Previewable) while the 
/// given condition is true nya
///
/// This requires a previewable iterator. If you can't use one, have a look at 
/// [eat_while].
pub fn eat_while_previewable<T: Previewable>(
    mut iterator: T, 
    mut cond: impl FnMut(&T::Item) -> bool
) -> T {
    while let Some(maybe_next) = iterator.preview() {
        if cond(maybe_next) {
            iterator.next();
        } else {
            break
        }
    };
    iterator
}

/// Consume elements from a non-previewable iterator until the given condition 
/// is true nyaaaaa.
///
/// Then spit out both the first non-matching element and the iterator. If you have
/// an iterator implementing [Previewable], check out
/// [eat_while_previewable].
pub fn eat_while <T: iter::Iterator>(
    mut iterator: T, 
    mut cond: impl FnMut(&T::Item) -> bool
) -> (Option::<T::Item>, T) {
    let mut next = iterator.next();
    while let Some(v) = &next {
        if !cond(v) {break};
        next = iterator.next();
    };
    (next, iterator)
    
}

#[cfg(test)]
mod test {

    #[test]
    pub fn eat_while_previewable() {
        let mut it = (0..10).peekable();
        it = super::eat_while_previewable(it, |a| *a < 4);
        assert_eq!(it.next(), Some(4))
    }

    #[test]
    pub fn eat_while() {
        // Non-empty next
        let (next, _) = super::eat_while(0..10, |a| *a < 4);
        assert_eq!(next, Some(4));
        // Empty next
        let (next, _) = super::eat_while(0..10, |a| *a < 11);
        assert_eq!(next, None);
    }
}
