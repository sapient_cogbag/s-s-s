//! Subcommands for basic html parsing of MMD documents nya

use crate::{
    branch_subcommand, 
    leaf_subcommand,
    subcommands,
    mmd
};

use subcommands::SubcommandLeafMaker;
use std::io::{stdin, Read};


leaf_subcommand!{
    pub ParseHTML ["html", "Take input on stdin and output HTML"] = fn (args) {
        let stored_args = args.collect::<Vec::<_>>();
        let (our_args, _) = rustop::opts! {
            command_name ParseHTML::NAME;
            synopsis ParseHTML::SHORT_INFO;
            opt basic_body:bool = false, desc:"Whether or not to include a basic HTML body in output";
        }.parse_args(stored_args.iter().map(|a| a.as_str()))
        .unwrap_or_else(|e| subcommands::help_and_exit(&e));

        // Get input!
        //
        // Add newline for front titles nya
        let mut input = "\n".to_owned();
        match stdin().lock().read_to_string(&mut input) {
            Ok(_) => {
                // Parse
                let parsed = mmd::parser::parse_document(&input)
                    .map(|a| mmd::repr::HTMLSanitizedDocument::from(a));
                match parsed {
                    Err(parse_err) => {
                        eprintln!("Parse error: {}", parse_err);
                        std::process::exit(2);
                    },
                    Ok(result) => {
                        let mut true_out = String::new(); 
                        result.append_html_to_string(&mut true_out);
                        if our_args.basic_body {
                            print!("<!DOCTYPE html>\n<html>\n<body>");
                        }
                        print!("{}", true_out);
                        if our_args.basic_body {
                            print!("\n</body>\n</html>");
                        }
                        std::process::exit(0);
                    }
                }
            },
            Err(e) => {
                eprintln!("{}", e);
                std::process::exit(1);
            }
        }
    }
}

branch_subcommand!{
    pub MMD,
    "mmd",
    "Subcommands to do with simple manipulations of mmd documents",
    leaf {
        ParseHTML;
    }
}
