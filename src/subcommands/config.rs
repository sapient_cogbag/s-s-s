//! Subcommand for general config manipulation/analysis
use crate::branch_subcommand;
use crate::{simple_config_selection_args, leaf_subcommand};
use crate::config;

leaf_subcommand!{
    /// Subcommand that displays the current overall config nya
    pub Show ["show", "Print the current config as read"] = fn (args) { 
        let (_, cfg, _) = simple_config_selection_args!(Show parsing args); 
        let txt = cfg.to_string();
        println!("{}", txt);
    }
}

leaf_subcommand!{
    /// Subcommand to create a default config in the local directory nya
    pub Generate [
        "generate", 
        "Generate a config file in the local config location"
    ] = fn (args) {
        let (_, _, local) = simple_config_selection_args!(Generate parsing args);
        config::make_local_config(local.as_deref(), false).map_err(|e|{
            eprintln!("{}", e);
            std::process::exit(1)
        }).unwrap()
    }
}

leaf_subcommand!{
    /// Subcommand to create a default local config and overwrite the current one
    /// if it exists nyaaaa
    pub GenerateOverwrite [
        "generate-overwrite", 
        "Generate a config file in the local config location, overwriting if necessary"
    ] = fn (args) { 
        let (_, _, local) = simple_config_selection_args!(GenerateOverwrite parsing args);
        config::make_local_config(local.as_deref(), true).map_err(|e|{
            eprintln!("{}", e.to_string());
            std::process::exit(1)
        }).unwrap()
    }
}

mod edit {
    use crate::{leaf_subcommand, simple_config_selection_args};
    use crate::config;
    use crate::subcommands::run_command_status;

    leaf_subcommand!{
        /// Edit the per-user config nya
        pub User ["user", "Edit your per-user config"] = fn (args) {
            let (maybe_xdg, cfg, _) = simple_config_selection_args!(User parsing args);
            maybe_xdg.map_or_else(|| {
                eprintln!("ERR [XDG]: Could not obtain XDG directories for per-user config.");
                std::process::exit(1);

            }, |xdg| {
                let cfg_file = xdg.find_config_file(config::XDG_CONFIG_FILE_NAME);
                cfg_file.map_or_else(|| { 
                    eprintln!("ERR [XDG]: Could not obtain path for XDG config file.");
                    std::process::exit(1);

                }, |path| {
                    path.to_str().map_or_else(|| {
                        eprintln!("ERR [PATH]: Unicode error.");
                        std::process::exit(1);

                    }, |final_path| {
                        let mut proc = std::process::Command::new(cfg.editor.editor_command);
                        proc.arg(final_path);
                        run_command_status(proc);
                    })
                })
            })
        }
    }

    leaf_subcommand!{
        /// Edit the local config
        pub Local ["local", "Edit the local config"] = fn (args) {
            let (_, cfg, local_config) = simple_config_selection_args!(Local parsing args);
            let mut proc = std::process::Command::new(cfg.editor.editor_command);
            proc.arg(local_config.unwrap_or(config::DEFAULT_LOCAL_CONFIG_FPATH.to_string()));
            run_command_status(proc);
        }
    }
}

branch_subcommand! {
    /// Edit the configs
    pub Edit,
    "edit",
    "Options for editing relevant config files",
    leaf {
        edit::User;
        edit::Local;
    }
}



branch_subcommand! {
    /// Some stuff for public configuration 
    pub Config,
    "config",
    "Options based around manipulating and showing the current configuration",
    leaf {
        Show;
        Generate;
        GenerateOverwrite;
    }
    branch {
        Edit;
    }
}


// s-s-s
// Copyright (C) 2021  sapient_cogbag <sapient_cogbag at protonmail.com>
//                                                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//                                                                       
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//                                                                       
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
