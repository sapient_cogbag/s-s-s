//! Actual MMD parser nya

use super::repr;
use std::iter::Iterator;
use seq_macro::seq;

macro_rules! next_k {
   ($vis:vis fn $name:ident $k:literal;) => {
        seq!{N in 0..$k {
            $vis fn $name <T> (iterator: impl ::std::iter::Iterator::<Item=T>) 
                -> (#(::std::option::Option::<T>,)*) {
                let mut final_none = iterator.take($k).fuse();
                let mut result: (#(::std::option::Option::<T>,)*) = (#(None,)*);
                #(result.N = final_none.next();)*
                result
            }
        }}
    }
}

next_k!{pub fn next_1 1;}
next_k!{pub fn next_2 2;}
next_k!{pub fn next_3 3;}
next_k!{pub fn next_4 4;}
next_k!{pub fn next_5 5;}

/// Parse a purely format block set without links or titles nya
///
/// This takes the initial format (for the continuation of previos segments)
/// and a chunk, and return a series of blocks as well as the new final format nya.
pub fn parse_non_link_title_blocks(
    chunk: &str, 
    initial_format: repr::BlockFormat
) -> (Vec::<repr::Block>, repr::BlockFormat) {
    let mut chars = chunk.chars();
    let mut seg: Vec::<repr::Block> = Vec::new();

    // Current chunk of data
    let mut curr_chunk = String::new();
    
    // Current format nyaaa
    let mut format = initial_format;
    // Currently in code or not
    let mut is_in_code: bool = false;

    
    loop {
        let next_4_chars = next_4(chars.clone());
        // Move forward one
        let next = chars.next();


        // Finalise the current block and wipe the current contents 
        macro_rules! finalise_current_block { () => {
            if is_in_code {
                seg.push(repr::Block::InlineCode(curr_chunk.clone()));
            } else {
                seg.push(repr::Block::Plain {
                    content: curr_chunk.clone(),
                    fmt: format.clone()
                });
            };
            curr_chunk.clear();
        }}
        
        // Skip a number of extra characters nya
        let mut skip_extra_chars = |count: usize| {
            for _ in 0..count {let u = chars.next(); if u == None {return;}};
        };

         // Continue on with the current set of characters
        // outside code
        if !is_in_code {
            match next_4_chars {
                // Process special chars
                (Some('~'), Some('~'), _, _) => {
                    finalise_current_block!();
                    skip_extra_chars(1);
                    format.toggle_strikethrough();
                }
                (Some('_'), Some('_'), Some('_'), _) 
                    | (Some('*'), Some('*'), Some('_'), _) => {
                    finalise_current_block!();
                    skip_extra_chars(2);
                    format.toggle_bold();
                    format.toggle_italics();
                }
                (Some('_'), Some('_'), _, _) | (Some('*'), Some('*'), _, _) => {
                    finalise_current_block!();
                    skip_extra_chars(1);
                    format.toggle_bold();
                }
                (Some('_' | '*'), _, _, _) => {
                    finalise_current_block!();
                    format.toggle_italics();
                }
                (Some('`'), _, _, _) => {
                    // Code
                    finalise_current_block!();
                    is_in_code = !is_in_code;
                }
                // Skipping special characters
                (Some('\\'), Some('~'), Some('~'), _) => {
                    skip_extra_chars(2);
                    curr_chunk.push_str("~~");
                }
                (Some('\\'), Some('_'), Some('_'), Some('_')) => {
                    skip_extra_chars(3);
                    curr_chunk.push_str("___"); 
                }
                (Some('\\'), Some('*'), Some('*'), Some('*')) => {
                    skip_extra_chars(3);
                    curr_chunk.push_str("***");
                }
                (Some('\\'), Some('_'), Some('_'), _) => {
                    skip_extra_chars(2);
                    curr_chunk.push_str("__"); 
                }
                (Some('\\'), Some('*'), Some('*'), _) => {
                    skip_extra_chars(2);
                    curr_chunk.push_str("**");
                }
                (Some('\\'), Some(ch @ ('*' | '_' | '`')), _, _) => {
                    skip_extra_chars(1);
                    curr_chunk.push(ch);
                }
                // Skipping other characters
                (Some('\\'), Some(ch), _, _) => {
                    skip_extra_chars(1);
                    curr_chunk.push(ch);
                },
                (Some(ch), _, _, _) => {
                    curr_chunk.push(ch);
                },
                // Final
                (None, _, _, _) => {
                    finalise_current_block!();
                    break
                }
            }
        } else {
            // inside code
            match next_4_chars {
                // Cancel the code-ness
                (Some('`'), _, _, _) => {
                    finalise_current_block!();
                    is_in_code = !is_in_code;
                }
                // Cancelling special char
                (Some('\\'), Some(ch), _, _) => {
                    skip_extra_chars(1);
                    curr_chunk.push(ch);

                }
                // Char forward
                (Some(ch), _, _, _) => {
                    curr_chunk.push(ch);
                }
                // Final
                (None, _, _, _) => {
                    finalise_current_block!();
                    break
                }
            }
        }

        if next == None {
            finalise_current_block!();
            break;
        }
    };
    (seg, format)
} 


#[derive(Clone, Eq, PartialEq)]
enum RegionToken {
    Plain,
    Newline,

    TitleID,
    TitleSize(repr::TitleSize),
    TitleEnd,
    
    LinkCover,
    LinkTarget,

    MultilineCode(String) // value is the lang
}

impl Default for RegionToken {
    fn default() -> Self {
        RegionToken::Plain
    }
}


/// Parse text into intermediary chunks.
///
/// Note that this will not work properly with titles on the first line
/// (due to lack of newline char nya) - recommendation is simply to
/// insert a newline character before all the other things happen nya
fn parse_blocks(raw_text: &str) -> Vec::<(String, RegionToken)> {
    let mut parse_chunks = Vec::<(String, RegionToken)>::new();

    #[derive(Default, Clone, Eq, PartialEq)]
    struct ContState {
        pub in_link: bool,
        pub in_title: bool,
        pub token: RegionToken,
    }

    
    let mut region_str = String::new();
    let mut state = ContState::default();

    enum GrandParseState {
        Normal,
        PrevWasDelim,
        MultilineCode
    }
    let mut grand_state = GrandParseState::Normal;

    let mut iterator = raw_text.chars().enumerate().fuse().peekable();


    loop {
        let next_4_elements_full = next_4(iterator.clone());
        let next_4_elements = seq!{N in 0..4 {
            (#(next_4_elements_full.N.map(|a| a.1),)*)
        }};


        // We depend on the loop branches and char eating to move us forward nya
        if next_4_elements.0 == None {
            break;
        }
        
        // Skip a number of extra characters nya
        macro_rules! skip_chars {($count: expr) => {
            for _ in 0..$count {
                let u = iterator.next(); 
                if u == None {
                    break;
                }
            };
        }}

        // Eat a number of characters into the region string nyaaaaa
        macro_rules! eat_chars {($count: expr) => {
            for _ in 0..$count {
                let u = iterator.next();
                match u {
                    Some(ch) => region_str.push(ch.1),
                    None => break
                }
            };
        }}

        macro_rules! skip_chars_until {($ch:ident, $cond:expr) => {
            loop {
                let u = iterator.peek().map(|a| *a);
                match u {
                    Some((_, $ch)) if $cond => break,
                    Some((_, _ch)) => {iterator.next();},
                    None => break
                }
            }
        }}

        // Eat chars until a specific char shows up
        // This does not include the char on the end 
        macro_rules! eat_chars_until {($cond:pat) => {
            loop {
                let u = iterator.peek().map(|a| *a);
                match u {
                    Some((_, $cond)) => break,
                    Some((_, ch)) => {
                        iterator.next();
                        region_str.push(ch);
                    },
                    None => break
                }
            }
        }}

        // This means the string should be empty at this point nya
        macro_rules! null_string {
            () => {region_str.clear()}
        }

        // Use a macro to enable continuous recapture nya
        macro_rules! finalise_chunk_initialise_new_region {
            ($skip_chars: expr, $initial_eat_chars: expr) => { 
                // Pop off the previous chunk (it was not in a link so it's 
                // plain
                parse_chunks.push((
                    region_str.clone(),
                    state.token.clone()
                ));
                skip_chars!($skip_chars);
                null_string!();
                eat_chars!($initial_eat_chars);
            };
            ($skip_chars: expr) => {
                finalise_chunk_initialise_new_region!($skip_chars, 0)
            }
        }


        match grand_state {
            GrandParseState::Normal => {
                // Do plain parsing
                match (next_4_elements, &state) {
                    // Only in a title and building a tag nya
                    ((Some('}'), _, _, _), ContState {
                        in_title: true,
                        token: RegionToken::TitleID,
                        ..
                    }) => {
                        // Put the contents.
                        finalise_chunk_initialise_new_region!(1);
                        // continue on with the title nyaaaa
                        state.token = RegionToken::Plain;
                        skip_chars_until!(ch, !ch.is_whitespace() || ch == '\n');
                    }
                    // The tag
                    ((Some('{'), _, _, _), ContState {
                        in_title: true,
                        token: RegionToken::TitleSize(_),
                        ..
                    }) => {
                        state.token = RegionToken::TitleID;
                        skip_chars!(1);
                    }
                    ((Some(_), _, _, _), ContState {
                        in_title: true,
                        token: RegionToken::TitleSize(_),
                        ..
                    }) => {
                        // change to to plain chunk
                        state.token = RegionToken::Plain;
                        null_string!();
                    }
                    ((Some('\n'), Some('#'), Some('#'), Some('#')), ContState {
                        in_title: false,
                        ..
                    }) => {
                        // Skip 4
                        finalise_chunk_initialise_new_region!(4); 
                        state.token = RegionToken::TitleSize(repr::TitleSize::Small);
                        state.in_title = true;
                        // dump token
                        finalise_chunk_initialise_new_region!(0);
                        skip_chars_until!(ch, !ch.is_whitespace() || ch == '\n');
                    }
                    ((Some('\n'), Some('#'), Some('#'), _), ContState {
                        in_title: false,
                        ..
                    }) => {
                        // Skip 3 nya
                        finalise_chunk_initialise_new_region!(3); 
                        state.token = RegionToken::TitleSize(repr::TitleSize::Medium);
                        state.in_title = true;
                        finalise_chunk_initialise_new_region!(0);
                        skip_chars_until!(ch, !ch.is_whitespace() || ch == '\n');
                    }
                    ((Some('\n'), Some('#'), _, _), ContState {
                        in_title: false,
                        ..
                    }) => {
                        // Skip 2
                        finalise_chunk_initialise_new_region!(2); 
                        state.token = RegionToken::TitleSize(repr::TitleSize::Large);
                        state.in_title = true;
                        finalise_chunk_initialise_new_region!(0);
                        skip_chars_until!(ch, !ch.is_whitespace() || ch == '\n');
                    }
                    // Multiline code.
                    ((Some('\n'), Some('`'), Some('`'), Some('`')), ContState {
                        in_title: false,
                        in_link: false, 
                        token: RegionToken::Plain
                    }) => {
                        // Use the region string char eating to get the language
                        finalise_chunk_initialise_new_region!(4);
                        eat_chars_until!('\n');
                        let lang = region_str.trim_start().trim_end().to_owned();
                        state.token = RegionToken::MultilineCode(lang);
                        // Clear the region string for proper parsing nya
                        null_string!();
                        // Go to code mode nya
                        grand_state = GrandParseState::MultilineCode;
                    }
                    ((Some('\n'), Some('\n'), _, _), ContState {
                        in_title: false,
                        ..
                    }) => {
                        // Inject newline token nya
                        finalise_chunk_initialise_new_region!(2);
                        state.token = RegionToken::Newline;
                        finalise_chunk_initialise_new_region!(0);
                        // Reset the token nya
                        state.token = RegionToken::Plain;
                    }
                    ((Some('\n'), _, _, _), ContState {
                        in_title: true,
                        ..
                    }) => {
                        // Finalise the current chunk - no skips in case of new title
                        finalise_chunk_initialise_new_region!(0);
                        // Generate the end point for the title
                        state.token = RegionToken::TitleEnd;
                        finalise_chunk_initialise_new_region!(0);
                        // end title 
                        state.in_title = false;
                        state.in_link = false;
                        state.token = RegionToken::Plain;
                        // Don't jump here, might be other title.
                    }
                    // Not in a link - this initiates a link
                    ((Some('['), Some(']'), Some('('), _), ContState {in_link: false, ..}) => {
                        finalise_chunk_initialise_new_region!(3); 
                        // Link target starts after bracket
                        state.in_link = true;
                        state.token = RegionToken::LinkTarget;

                    }
                    ((Some('['), _, _, _), ContState {in_link: false, ..}) => {
                        finalise_chunk_initialise_new_region!(1); 
                        // Content starts one after
                        state.in_link = true;
                        state.token = RegionToken::LinkCover;
                    },
                    ((Some(']'), Some('('), _, _), ContState {in_link: true, ..}) => {
                        finalise_chunk_initialise_new_region!(2); // Link target starts after (
                        state.token = RegionToken::LinkTarget;
                    }
                    ((Some(')'), _, _, _), ContState {
                        in_link: true, 
                        token: RegionToken::LinkTarget,
                        ..
                    }) => {
                        finalise_chunk_initialise_new_region!(1);
                        state.in_link = false;
                        state.token = RegionToken::Plain;
                    }
                    ((Some('\\'), _, _, _), _) => {
                        grand_state = GrandParseState::PrevWasDelim;
                        skip_chars!(1);
                    }
                    ((Some(_), _, _, _), _) => {
                        eat_chars!(1);
                    }
                    ((None, _, _, _), _) => break,
                }
            }
            GrandParseState::PrevWasDelim => {
                match next_4_elements {
                    // Special characters at this level - skip over them
                    // so they don't trigger any new chunks
                    //
                    // this sets the next end idx to the current thing (skipping it)
                    (Some(_), _, _, _) => {
                        grand_state = GrandParseState::Normal;
                        eat_chars!(1);
                    }
                    (None, _, _, _) => break,
                }
            }
            GrandParseState::MultilineCode => {
                match next_4_elements {
                    // In case of ``` on line, finalise and transition back to newline nya
                    (Some('\n'), Some('`'), Some('`'), Some('`')) => {
                        finalise_chunk_initialise_new_region!(4);
                        state.token = RegionToken::Plain;
                        grand_state = GrandParseState::Normal;
                    },
                    // Else, eat the characters nya
                    (Some(_), _, _, _) => {
                        eat_chars!(1);
                    },
                    (None, _, _, _) => break
                }
            }
        }
    };
    // Final thing
    parse_chunks.push((
        region_str,
        state.token
    ));
    parse_chunks
}

// Parse a set of intermediate tokens
fn sub_parse_intermediates(tokens: Vec::<(String, RegionToken)>) 
    -> Result::<repr::Document, String> {
    let mut iter = tokens.into_iter().peekable();
    let mut doc = repr::Document::default();
    let mut format = repr::BlockFormat::default();


    // Scan for purely plain things nya
    //
    // Spits out a vec of blocks
    macro_rules! scan_plain_only {
        () => {{
            let mut blocks = Vec::<repr::Block>::new();
            while let Some((_, ty)) = iter.peek() {
                match ty {
                    RegionToken::Plain => {
                        let (data, _) = iter.next().expect("Some.");
                        let (chunk_blocks, new_format) = 
                            parse_non_link_title_blocks(&data, format.clone());
                        blocks.extend(chunk_blocks.into_iter());
                        format = new_format;
                    }
                    RegionToken::MultilineCode(_) => {
                        match iter.next().unwrap() {
                            (data, RegionToken::MultilineCode(lang)) => match lang.as_str() {
                                "" => blocks.push(repr::Block::Code {
                                    content: data,
                                    language: None
                                }),
                                _ => blocks.push(repr::Block::Code {
                                    content: data,
                                    language: Some(lang)
                                })
                            },
                            _ => unreachable!("We are already inside something matching")
                        }
                    }
                    _ => break
                }
            };
            blocks
        }}
    }

    // Scan for links. Note that if a cover exists and there is anything other
    // than a link target afterward it gets retransformed into a block containing 
    // "[" + ... + "]"
    macro_rules! scan_link {
        () => {{
            // Grab the cover nya
            let mut cover_blocks = Vec::<repr::Block>::new();
            if let Some((_, RegionToken::LinkCover)) = iter.peek() {
                let (to_parse, _) = iter.next().unwrap();
                let (parsed_blocks, new_format) 
                    = parse_non_link_title_blocks(&to_parse, format);
                format = new_format;
                cover_blocks = parsed_blocks;
            }
            
            // Try and grab a target. If none, we spit out a block with "["
            let mut target = Option::<String>::None;
            if let Some((_, RegionToken::LinkTarget)) = iter.peek() {
                let (targ, _) = iter.next().unwrap();
                target = Some(targ);
            };
            
            match target {
                Some(is_url_target) => {
                    let t = is_url_target.trim_start().trim_end().to_owned();
                    repr::LinkBlock::LinkBlocks {
                        target: t,
                        blocks: {
                            if cover_blocks.len() == 0 {
                                // No cover blocks BUT we still indicate that
                                // there was a previous newline with \n\n on the
                                // target (accounted for in nya LinkBlock::append_html_to_str)
                                None
                            } else {
                                Some(cover_blocks)
                            }
                        }
                    }
                },
                None => {
                    // Not actual link nya
                    //
                    // When you get a cover without a target, the final ] is 
                    // not skipped over as it is with a target - so no need to 
                    // add it nya
                    if cover_blocks.len() > 0 {
                        cover_blocks[0].content_mut().insert(0, '[');
                    }
                    repr::LinkBlock::Blocks(cover_blocks)
                }
            }
        }}
    }

    macro_rules! scan_plains_or_links {
        () => {{
            let mut res = Vec::<repr::LinkBlock>::new();
            while let Some((_, 
                RegionToken::Plain |
                RegionToken::MultilineCode(_) |

                RegionToken::LinkCover | 
                RegionToken::LinkTarget
            )) = iter.peek() {
                let plains = scan_plain_only!();
                if plains.len() != 0 {
                    res.push(repr::LinkBlock::Blocks(plains));
                }
                
                let links = scan_link!();
                if !links.is_empty() {res.push(links)};
            };
            res
        }}
    }

    // Scan a single title nya
    macro_rules! scan_title_or_other {
        () => {{
            let mut title: Option::<repr::Title> = None;
            if let Some((_, RegionToken::TitleSize(sz))) = iter.peek() {
                title = Some(repr::Title {
                    size: *sz,
                    fragment_id: None
                });
                iter.next();
            }

            if title.is_some() {
                // titles render this completely irrelevant nya
                if let Some((_fragid, RegionToken::TitleID)) = iter.peek() {
                    let fragid = iter.next().unwrap().0.trim_start().trim_end().to_owned();
                    title = title.map(|a| repr::Title {
                        fragment_id: Some(fragid),
                        ..a
                    });
                }
            }

            // Attempt to run to title end
            let block_contents = scan_plains_or_links!();

            // If we are at the end of a title or we have a newline then 
            // eat those before making a new segment nya
            if let Some((_, RegionToken::TitleEnd | RegionToken::Newline)) 
                = iter.peek() { iter.next(); }

            repr::Segment {
                link_blocks: block_contents,
                title
            }
        }}
    }

    while iter.peek().is_some() {
        doc.data.push(scan_title_or_other!())
    };

    Ok(doc)
}

/// Convert a string into MMD structure nya nyaa
pub fn parse_document(raw_text: &str) -> Result::<repr::Document, String> {
    let chunks = parse_blocks(raw_text);
    sub_parse_intermediates(chunks)
}






// s-s-s
// Copyright (C) 2021  sapient_cogbag <sapient_cogbag at protonmail.com>
//                                                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//                                                                       
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//                                                                       
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
