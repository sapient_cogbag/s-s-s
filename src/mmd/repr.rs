//! For the internal representation of modified markdown nya

#[derive(Copy, Clone)]
pub struct BlockFormat {
    pub bold: bool,
    pub italics: bool,
    pub strikethrough: bool
}

impl BlockFormat {
    pub fn toggle_bold(&mut self) {
        self.bold = !self.bold;
    }
    pub fn toggle_italics(&mut self) {
        self.italics = !self.italics;
    }
    pub fn toggle_strikethrough(&mut self) {
        self.strikethrough = !self.strikethrough;
    }

    /// Append the beginning tags nya
    pub fn append_start_tags(&self, out: &mut String) {
        // Bold is 3: '<b>'
        // italics is 3: '<i>'
        // strikethrough is 45: '<span style="text-decoration:line-through">"'
        // ⅀ = 51
        out.reserve(51);
        if self.bold {
            out.push_str("<b>");
        }
        if self.italics {
            out.push_str("<i>");
        }
        if self.strikethrough {
            out.push_str("<span style=\"text-decoration:line-through\">");
        }
    }

    /// Append the end tags
    pub fn append_end_tags(&self, out: &mut String) {
        // 4 + 4 + 7, so total is 15
        out.reserve(12);
        if self.strikethrough {
                out.push_str("</span>");
        }
        if self.italics {
            out.push_str("</i>");
        }
        if self.bold {
            out.push_str("</b>");
        }
    }
}

impl Default for BlockFormat {
    fn default() -> Self {
        Self {
            bold: false,
            italics: false,
            strikethrough: false
        }
    }
}


/// STRUCTURE:
/// * Titles in MMD are marked with one, two, or three ###'s before the
/// actual title. They can be prefixed with a {} containing a single string 
/// to essentially make a URL-fragment (defines the id element) nya e.g:
///    ### {.start} Title
///
/// The smaller the number of #, the larger the title is.
///
/// All other data is in chunked blocks nya. Blocks can have tags for formatting
/// (in particular bold and italics)
///
/// Titles themselves are a set of blocks, just like the rest of the text is nya
///
/// The text of a link is a set of blocks of its own, 
///
/// Code blocks are special types which result in all other formatting being ignored, and
/// can contain newlines etc. nya. Inline code is similar except with no newlines.
///
/// Out of line code is delimited with a single line of ```
/// on each side, with an optional language specified after ```
///
/// Inline code is delimited by `... code ...`
///
/// To input a newline, simply put two empty lines. Otherwise the paragraphs will be parsed 
/// as a single one with a space between the end of one and the start of another nya
///
/// Italics is delimited by one * or _
/// Bold is delimited by two ** or __
/// Bold+Italics is delimited by *** or ___
/// Strikethrough is delimited by ~~
///
/// These have to match so you can't partially toggle continuously nya
///
/// Backslash prevents the parsing of the next non-whitespace batch of characters as special
/// nya
pub enum Block {
    Plain {
        content: String,
        fmt: BlockFormat
    },
    Code {
        content: String,
        language: Option::<String>
    },
    InlineCode(String),
}

impl Block {
    pub fn content(&self) -> &str {
        match self {
            Block::Plain { content, .. } => content,
            Block::Code { content, .. } => content,
            Block::InlineCode( content ) => content
        }
    }

    pub fn content_mut(&mut self) -> &mut String {
        match self {
            Block::Plain { content, .. } => content,
            Block::Code { content, .. } => content,
            Block::InlineCode( content ) => content
        }
    }

    /// Sanitize content for HTML nya
    pub fn sanitize_html(&mut self) {
        // Non-inline code does not get sanitised since it is 
        // put inside a <pre> block nya
        //
        // Also we scan for double-newlines and replace with <br/>
        match self {
            Self::Plain {content, ..} => *content = ammonia::clean_text(&content),
            Self::InlineCode (content) => *content = ammonia::clean_text(&content),
            Self::Code { .. } => {}
        }
    }

    /// append HTML to a string with appropriate tags nya
    pub fn append_html_to_string(&self, out_block: &mut String) {
        match self {
            Block::Plain { content, fmt } => {
                if content.trim().len() != 0 {
                    fmt.append_start_tags(out_block);
                    out_block.push_str(&content);
                    fmt.append_end_tags(out_block);
                }

            }
            // Non-inline: we use <pre></pre>
            Block::Code { content, language } => {
                match language {
                    Some(lang) => {
                        out_block.push_str("<code class=\"");
                        out_block.push_str(&lang);
                        out_block.push_str("\"><pre>");
                    },
                    None => out_block.push_str("<code><pre>")
                }
                out_block.push_str(&content);
                out_block.push_str("</pre></code>");
            }
            Block::InlineCode(content) => {
                out_block.push_str("<code>");
                out_block.push_str(&content);
                out_block.push_str("</code>");
            }
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum TitleSize {
    Small,
    Medium,
    Large
}


pub enum LinkBlock {
    Blocks(Vec::<Block>),
    LinkBlocks {
        target: String,
        blocks: Option::<Vec::<Block>>
    }
}

impl LinkBlock {
    /// Whether this holds anything or not
    pub fn is_empty(&self) -> bool {
        match self {
            Self::Blocks(v) => v.len() == 0,
            Self::LinkBlocks {..} => false
        }
    }

    /// Sanitize for html
    pub fn sanitize_html(&mut self) {
        match self {
            Self::Blocks(v) => {
                for item in v {
                    item.sanitize_html();
                }
            },
            Self::LinkBlocks {
                blocks: Some(blk),
                ..
            } => {
                for item in blk {
                    item.sanitize_html();
                }
            },
            // No content for sanitization nyaaaaeoww
            _ => {}
        }
    }

    /// Append to a string with HTML
    pub fn append_html_to_string(&self, out: &mut String) {
        match self {
            Self::Blocks(v) => {
                v.iter().for_each(|a| a.append_html_to_string(out));
            },
            Self::LinkBlocks {
                blocks: Some(v),
                target
            } => {
                out.push_str("<a href=\"");
                out.push_str(target.trim_start().trim_end());
                out.push_str("\">");
                v.iter().for_each(|a| a.append_html_to_string(out));
                out.push_str("</a>");
            }
            Self::LinkBlocks {
                blocks: None,
                target
            } => {
                out.push_str("<a href=\"");
                out.push_str(target.trim_start().trim_end());
                out.push_str("\">");
                out.push_str(&ammonia::clean_text(&target));
                out.push_str("</a>");
            }
        }
    }
}

/// Extra info about a title

pub struct Title {
    pub size: TitleSize,
    pub fragment_id: Option::<String>
}

impl Title {
    /// Append start tags to a string
    pub fn append_start_html_tags(&self, out: &mut String) {
        match &self.fragment_id {
            Some(id) => {
                match self.size {
                    TitleSize::Small => out.push_str("<h6 id=\""),
                    TitleSize::Medium => out.push_str("<h3 id=\""),
                    TitleSize::Large => out.push_str("<h1 id=\"")
                };
                out.push_str(&id);
                out.push_str("\">");
            },
            None => match self.size {
                TitleSize::Small => out.push_str("<h6>"),
                TitleSize::Medium => out.push_str("<h3>"),
                TitleSize::Large => out.push_str("<h1>")
            }
        }
    }

    /// Append end tags to a string nya
    pub fn append_end_html_tags(&self, out: &mut String) {
        match self.size {
            TitleSize::Small => out.push_str("</h6>\n"),
            TitleSize::Medium => out.push_str("</h3>\n"),
            TitleSize::Large => out.push_str("</h1>\n")
        }
    }
}

pub struct Segment {
    pub link_blocks: Vec::<LinkBlock>, 
    pub title: Option::<Title>
}

impl Segment {
    pub fn sanitize_html(&mut self) {
        for item in &mut self.link_blocks {
            item.sanitize_html();
        }
    }

    /// NYAAA!
    pub fn append_html_to_string(&self, out: &mut String) {
        match &self.title {
            Some(title) => {
                title.append_start_html_tags(out);
                self.link_blocks.iter().for_each(|a| a.append_html_to_string(out));
                title.append_end_html_tags(out);
            },
            None => {
                out.push_str("<p>\n");
                self.link_blocks.iter().for_each(|a| a.append_html_to_string(out));
                out.push_str("\n</p>\n")
            }
        }
    }
}

#[derive(Default)]
pub struct Document {
    pub data: Vec::<Segment>
}

pub struct HTMLSanitizedDocument {
    data: Vec::<Segment>
}

impl HTMLSanitizedDocument {
    pub fn append_html_to_string(&self, out: &mut String) {
        self.data.iter().for_each(|seg| seg.append_html_to_string(out));
    }
}

impl From::<Document> for HTMLSanitizedDocument {
    fn from(doc: Document) -> Self {
        Self {
            data: doc.data.into_iter()
                .map(|mut seg| {seg.sanitize_html(); seg})
                .collect()
        }
    }
}





// s-s-s
// Copyright (C) 2021  sapient_cogbag <sapient_cogbag at protonmail.com>
//                                                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//                                                                       
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//                                                                       
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
