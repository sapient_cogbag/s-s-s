//! Holds different subcommands
use std::hash::Hash;

pub mod config;
pub mod mmd;
use rustop::{Error, error_and_exit};

/// Subcommand function
///
/// First argument is the name of the subcommand
/// Second argument is a short description/help/info of it nya
/// Final argument is an iterator over the arguments provided.
type SubcommandFunctionPtr = 
    fn(&'static str, &'static str, Box::<dyn Iterator::<Item=String>>) -> ();

/// Struct for actual subcommands - subcommands can either hold other subcommands
/// or actually be a command
#[derive(PartialEq, Eq)]
pub struct SubcommandLeaf {
    /// Actually run the command nya
    ///
    /// First argument is the name of the command,
    /// second is short info
    /// third is the argument parameters
    command: SubcommandFunctionPtr,

    /// Command name
    name: &'static str,

    /// synopsis of the subcommand
    short_help: &'static str
}



impl SubcommandLeaf {
    /// Make a new subcommand nya
    pub fn new(
        name: &'static str, 
        short_info: &'static str, 
        command: SubcommandFunctionPtr
    ) -> Self {
        Self {
            name,
            short_help: short_info,
            command
        }
    }

    /// Print out the help
    ///
    /// Literally just passes --help as an argument to the command function nya
    pub fn print_help(&self) {
        let help_arg = vec!["--help".to_string()];
        
        // Dump out the help nya
        (self.command)(self.name, self.short_help, Box::new(help_arg.into_iter()));
    }
}

impl Hash for SubcommandLeaf {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.short_help.hash(state);
        Hash::hash(&self.command, state);
    }
}



/// Holds a subcommand that only holds other subcommands nya
#[derive(PartialEq, Eq)]
pub struct SubcommandBranch { 
    name: &'static str,
    info: &'static str,
    subcommands: std::collections::HashSet::<Subcommand>
}

impl SubcommandBranch {
    /// Create an empty set of sub-subcommands
    pub fn new(name: &'static str, info: &'static str) -> Self {
        Self {
            name,
            info,
            subcommands: Default::default()
        }
    }

    /// Add another subcommand to this one nya
    ///
    /// If the subcommand already existed, return true. Else, return false.
    pub fn register_subcommand<S: Into::<Subcommand>>(&mut self, subcommand: S) -> bool {
        self.subcommands.insert(subcommand.into())
    }

    /// Print help
    pub fn print_help(&self) {
        eprintln!("- {} -\n{}\n\nSubcommands:", self.name, self.info);
        for sc in self.subcommands.iter() {
            eprintln!(" * {}: {}", sc.name(), sc.short_description());  
        }
    }
}

impl Hash for SubcommandBranch {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) { 
        self.name.hash(state);
        self.info.hash(state);
        for ele in self.subcommands.iter() {
            ele.hash(state); 
        }
    }
}




/// Holds a subcommand that is either an actual subcommand or only holds other 
/// subcommands nya
pub enum Subcommand {
    Branch (SubcommandBranch),
    Leaf (SubcommandLeaf)
}

impl PartialEq for Subcommand {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Subcommand::Branch(a), Subcommand::Branch(b)) => a == b,
            (Subcommand::Leaf(a), Subcommand::Leaf(b)) => a == b,
            _ => false
        }
    }
}

impl Eq for Subcommand {}

impl From::<SubcommandLeaf> for Subcommand {
    fn from(d: SubcommandLeaf) -> Self {
        Subcommand::Leaf(d)
    }
}

impl From::<SubcommandBranch> for Subcommand {
    fn from(d: SubcommandBranch) -> Self {
        Subcommand::Branch(d)
    }
}



impl Subcommand {
    /// Get the name of the subcommand
    pub fn name(&self) -> &'static str {
        match self {
            Subcommand::Branch (SubcommandBranch {name, ..}) => name,
            Subcommand::Leaf(sc) => sc.name
        }
    }

    /// Get the short description
    pub fn short_description(&self) -> &'static str {
        match self {
            Subcommand::Branch (SubcommandBranch {info, ..}) => info,
            Subcommand::Leaf(sc) => sc.short_help
        }
    }

    /// Print out full help. This is basically --help
    /// (prints onto stderr)
    pub fn help(&self) {
        match self {
            Subcommand::Branch (br) => {
                br.print_help();
            }
            Subcommand::Leaf(sc) => {
                sc.print_help();
            }
        }
    }
}

impl std::hash::Hash for Subcommand {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            Self::Branch (br) => {
                br.hash(state);
                state.write_u8(0);
            }
            Self::Leaf(sc) => {
                sc.hash(state);
                state.write_u8(1);
            }
        }
    }
}


/// Holds the result of a partial subcommand search.
///
/// Either it is complete (and has a real subcommand contained within)
/// or it is incomplete and has another reference to a branch inside nya,
/// or the provided argument is invalid
pub enum SCSearchResult<'a> {
    /// Search is complete
    Complete(&'a SubcommandLeaf),
    /// Moving down the branch was successful but there are still more branches to go
    /// nya
    Incomplete(&'a SubcommandBranch),
    /// There was no subcommand by the given name in the branch (holds the failed
    /// name)
    Failed(String) 
}

/// Move down the Subcommand Tree one level, looking for another command
pub fn locate_command<'a>(
    current_branch: &'a SubcommandBranch, 
    name: String
) -> SCSearchResult::<'a> {
    for item in current_branch.subcommands.iter() {
        // In case of match actually do something nya
        if item.name() == name.as_str() {
            match item {
                Subcommand::Branch(br) => {
                    return SCSearchResult::Incomplete(br);
                }
                Subcommand::Leaf(sc) => {
                    return SCSearchResult::Complete(sc);
                }
            }
        }
    }
    SCSearchResult::Failed(name)
}

/// Run some subcommands! with arguments and messages! nyaaa!
pub fn run_command<'a>(
    commands: &'a Subcommand, 
    mut args: Box::<dyn Iterator::<Item=String>>
) {
    match commands {
        Subcommand::Branch(br) => {
            let mut current_branch = br;
            loop {
                let next_arg = args.next();

                // Attempt to get another argument
                if let Some(nxt) = next_arg {
                    // Attempt to locate the next region nyaa
                    let search_result = locate_command(
                        current_branch,
                        nxt
                    );
                    match search_result {
                        // We have an actual command nyaaa, run it and exit
                        SCSearchResult::Complete(leaf) => {
                            (leaf.command)(leaf.name, leaf.short_help, args);
                            break;
                        }
                        SCSearchResult::Incomplete(branch) => {
                            // Update to the next region of the search
                            current_branch = branch;
                        }
                        // Failure! Print error and help
                        SCSearchResult::Failed(failed_name) => {
                            eprintln!("No such subcommand '{}'\n", failed_name);
                            current_branch.print_help();
                            break;
                        }
                    }
                     
                } else {
                    // No way to find command => print help
                    current_branch.print_help();
                    break;
                }
            }
        }
        Subcommand::Leaf(le) => {
            (le.command)(le.name, le.short_help, args); 
        }
    }
}

/// A simple trait to build actual subcommands with nya
pub trait SubcommandLeafMaker {
    const NAME: &'static str;
    const SHORT_INFO: &'static str;

    fn command (
        name: &'static str, 
        short_info: &'static str, 
        args: Box::<dyn Iterator::<Item=String>>
    ) -> ();
}

/// Turn a static leaf subcommand specification into an actual subcommand nyaa
pub fn make_leaf_subcommand<SC: SubcommandLeafMaker>() -> Subcommand {
    SubcommandLeaf::new(SC::NAME, SC::SHORT_INFO, SC::command).into()
}

/// For use with the branch_subcommand macro.
pub trait SubcommandBranchMaker {
    const NAME: &'static str;
    const SHORT_INFO: &'static str;

    /// make and add all the subcommands nya.
    fn make_subcommands(sc: SubcommandBranch) -> SubcommandBranch;
}

pub fn make_branch_subcommand<SC: SubcommandBranchMaker>() -> Subcommand {
    SC::make_subcommands(SubcommandBranch::new(SC::NAME, SC::SHORT_INFO)).into()
}

/// Macro for generating branch subcommand maker
#[macro_export] 
macro_rules! branch_subcommand {
    (
        $(#[$meta:meta])*
        pub $maker_name:ident,
        $command_name:literal,
        $short_info:literal,
        $(leaf {
        $($subcommand_leaf_maker:ty;)*})?
        $(branch {
        $($subcommand_branch_maker:ty;)*})?
    ) => {
        $(#[$meta])*
        pub struct $maker_name ();

        impl $crate::subcommands::SubcommandBranchMaker for $maker_name {
            const NAME: &'static str = $command_name;
            const SHORT_INFO: &'static str = $short_info;

            fn make_subcommands(
                mut sc: $crate::subcommands::SubcommandBranch
            ) -> $crate::subcommands::SubcommandBranch {
                $($(
                    sc.register_subcommand(
                        $crate::subcommands::make_leaf_subcommand::<$subcommand_leaf_maker>()
                    ); 
                )*)?
                $($(
                    sc.register_subcommand(
                        $crate::subcommands::make_branch_subcommand::<$subcommand_branch_maker>()
                    );
                )*)?
                sc
            }
        }
    }
}


/// Macro for making leaf subcommands nyaaa ^.^
#[macro_export]
macro_rules! leaf_subcommand {
    {
        $(#[$meta:meta])*
        pub $maker_name:ident [$command_name:literal, $short_info:literal] 
            = fn ($args:ident) $command:block
    } => {
        $(#[$meta])*
        pub struct $maker_name();

        impl $crate::subcommands::SubcommandLeafMaker for $maker_name { 
            const NAME: &'static str = $command_name;
            const SHORT_INFO: &'static str = $short_info;
            
            $(#[$meta])*
            fn command (
                _: &'static str, 
                _: &'static str, 
                $args: ::std::boxed::Box::<dyn std::iter::Iterator::<Item=::std::string::String>>
            ) -> () $command
        }
    }
}


/// Better version of the function from rustop which actually displays a help message if required
/// nya
pub fn help_and_exit(err: &Error) -> ! {
    if let Error::Help(msg) = err {
        eprintln!("{}", msg);
        std::process::exit(1);
    } else {
        error_and_exit(err);
    }
}


/// Simple wrapper for the opts! interface that loads the config/xdg and also
/// handles errors and the like nya.
///
/// This also makes available the SubcommandLeafMaker trait nyaaaa
///
/// Returns a quadruplet of (xdg basedirs, config, local config, args)
#[macro_export]
macro_rules! standard_config_parameters_and_xdg {
    ($args_iter:ident with config_options $opts:tt) => {{
        use $crate::subcommands::SubcommandLeafMaker;
        // Store the arguments
        let stored_args = $args_iter.collect::<::std::vec::Vec::<_>>();
        let (our_args, _) = rustop::opts!$opts
            .parse_args(stored_args.iter().map(|a| a.as_str()))
            .unwrap_or_else(|e| $crate::subcommands::help_and_exit(&e));
        let xdg = $crate::config::get_xdg();
        let loc = our_args.local_config.clone();
        let cfg = $crate::config::load_config(&xdg, loc.as_deref());
        (xdg, cfg, loc, our_args)
    }}
}


/// Provides some sensible defaults for plainly allowing specification of local config ^.^
///
/// Returns the (xdg basedirs, config, local config) triplet!
#[macro_export]
macro_rules! simple_config_selection_args {
    ($maker:ident parsing $args:ident) => {
        {
            let (xdg, cfg, loc, _) = $crate::standard_config_parameters_and_xdg! {
                $args with config_options {
                    command_name $maker::NAME;
                    synopsis $maker::SHORT_INFO;
                    opt local_config:Option<String> = None, desc:"Local/override config file";
                }
            };
            (xdg, cfg, loc)
        }
    }
}

/// Nice utility function for running a clean command with exiting nya
pub fn run_command_status(mut cmd: std::process::Command) {
    let r = cmd.status();
    match r {
        Ok(status) => {
            if status.success() {
                std::process::exit(0)
            } else {
                eprintln!(
                    "ERR [PROC]: Process exited unsuccessfully: {}", 
                    status.to_string()
                );
                std::process::exit(1);
            }
        }
        Err(err) => {
            eprintln!("ERR [PROC]: Unable to start process: {}", err.to_string());
            std::process::exit(1);
        }
    }
} 


// s-s-s
// Copyright (C) 2021  sapient_cogbag <sapient_cogbag at protonmail.com>
//                                                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//                                                                       
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//                                                                       
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
